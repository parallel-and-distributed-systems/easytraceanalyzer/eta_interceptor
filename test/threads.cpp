#include <chrono>
#include <iostream>
#include <mutex>
#include <sstream>
#include <thread>
#include <vector>

auto main() -> int {
    auto threads = std::vector<std::thread> {};
    auto constexpr thread_count = 30u;
    threads.reserve(thread_count);

    auto sum = std::size_t(0u);
    auto mtx = std::mutex {};

    for (auto i = 0u; i < thread_count; ++i) {
        threads.emplace_back([&]() {
            auto id = [&]() {
                auto ss = std::stringstream {};
                ss << std::this_thread::get_id();
                return std::stoll(ss.str());
            }();
            std::this_thread::sleep_for(std::chrono::seconds(2));
            auto lck = std::unique_lock { mtx };
            sum += id;
        });
    }

    for (auto & t : threads)
        t.join();

    std::cout << sum << std::endl;
}

