using JSON

@assert length(ARGS) >= 3
mode = ARGS[1]
destination = ARGS[2]
src = ARGS[3:length(ARGS)]

# Configuration

const functionPtrPrefix = "orig_"
const eventEnumName = "FunctionEvent"
const eventEnumPrefix = "eta_"

# Types

struct Argument
    name::String
    type::String
    is_array::Bool
    is_fn_ptr::Bool
end

struct Function
    name::String
    args::Vector{Argument}
    ret::String
    is_variadic::Bool
    is_manual::Bool
end

struct InterceptionModule
    name::String
    functions::Vector{Function}
    extra::Vector{String}
    includes::Vector{String}
end

# Utils

enterEvent(f::Function) = "$(eventEnumPrefix)$(f.name)"
eventName(f::Function) = f.name
exitEvent(f::Function) = "$(eventEnumPrefix)$(f.name)_exit"
functionPtrName(f::Function) = functionPtrPrefix * f.name
initPtr(f::Function) = "$(functionPtrName(f)) = ($(fn_ptr_type(f))) dlsym(RTLD_NEXT, \"$(f.name)\")"

function getArgumentType(a::Argument)
    if a.is_fn_ptr || !a.is_array
        a.type
    else
        "$(a.type)[]"
    end
end

function getArgumentName(a::Argument)
    a.name
end

function getArgument(a::Argument)
    if a.is_fn_ptr
        replace(a.type, r"\(\*\)" => "(*$(a.name))")
    elseif a.is_array
        "$(a.type) $(a.name)[]"
    else
        "$(a.type) $(a.name)"
    end
end

function getArgumentsTypes(f::Function)
    if length(f.args) == 0
        "void"
    else
        f.args .|> getArgumentType |> (x -> join(x, ", "))
    end
end

function getArguments(f::Function)
    if length(f.args) == 0
        "void"
    else
        f.args .|> getArgument |> (x -> join(x, ", "))
    end
end

function getArgumentsNames(f::Function)
    f.args .|> getArgumentName |> (x -> join(x, ", "))
end

# parsing

modules = (src .|>
           (path -> open(path) do f; read(f, String) end) .|>
           (JSON.parse) .|>
           (json -> InterceptionModule(json["name"],
                           (json["functions"] |>
                            (x -> filter(x) do (n, d); get(d, "active", true) end) |>
                            (x -> [ Function(name,
                                             (description["args"] .|>
                                              x -> Argument(x["name"],
                                                            x["type"],
                                                            get(x, "array", false),
                                                            get(x, "fn_ptr", false))),
                                             description["ret"],
                                             get(description, "variadic", false),
                                             get(description, "manual", false))
                                   for (name, description) in x])),
                           json["extra"],
                           json["includes"])))

functions = modules .|> (x -> x.functions) |> (x -> vcat(x...))


if mode == "events_header"
    """
    #ifndef ETA_INTERCEPT_EVENTS_H
    #define ETA_INTERCEPT_EVENTS_H

    static int const eta_intercept_event_count = $(length(functions));

    enum $(eventEnumName) {
    $([ "    $(enterEvent(fn)),"
       for fn in functions
      ] |> (x -> join(x, "\n")))
    };

    #endif // ETA_INTERCEPT_EVENTS_H
    """ |> (x -> open(destination, "w") do output; write(output, x) end)

elseif mode == "events_source"
    """
    #ifndef ETA_INTERCEPT_EVENT_NAMES_H
    #define ETA_INTERCEPT_EVENT_NAMES_H

    static char const * get_event_name(FunctionEvent e) {
        switch (e) {
        $(functions .|>
          (fn -> """
                   case $(enterEvent(fn)): return \"$(eventName(fn))\";
           """) |>
          join)
        }
    }
    #endif // ETA_INTERCEPT_EVENT_NAMES_H
    """ |> (x -> open(destination, "w") do output; write(output, x) end)

elseif mode == "modules_header"
    """
    #ifndef ETA_INTERCEPT_MODULES_H
    #define ETA_INTERCEPT_MODULES_H

    struct eta_oracle_module {
        char const * name;
        char const * description;
    };

    static int const eta_oracle_module_count = $(length(modules));

    struct eta_oracle_module eta_oracle_modules[] = {
    $(modules .|>
      (m -> "    { \"$(m.name)\", \"Enable module $(m.name)\" }") |>
      (x -> join(x, ",\n")))
    };

    #endif // ETA_INTERCEPT_MODULES_H
    """ |> (x -> open(destination, "w") do output; write(output, x) end)

elseif mode == "module"
    @assert length(modules) == 1
    m = modules[1]

    ret_storage(f) = if f.ret == "void"; "" else "$(f.ret) ret = " end
    fn_ptr_type(f) = "$(f.ret)(*)($(getArgumentsTypes(f)))"

    """
    #include "eta_oracle_events.h"

    #define _GNU_SOURCE
    #include <assert.h>
    #include <dlfcn.h>
    #include <stddef.h>

    #include <eta/oracle/interceptor.h>

    // Includes
    $(m.includes .|> (i -> "\n#include <$(i)>") |> join)

    // Extra definitions

    $(m.extra |> (x -> join(x, "\n")))

    // Function declarations

    // Pointers on intercepted functions

    $(filter(m.functions) do f;
          !f.is_variadic && !f.is_manual
      end .|> (f -> """
               $(f.ret) $(f.name)($(getArguments(f))) {
                   if (!eta_is_in_recursion()) {
                       eta_prevent_recursion_enter();
                       eta_intercept_event($(enterEvent(f)));
                       eta_prevent_recursion_exit();
                   }

                   static $(f.ret) (*$(functionPtrName(f)))($(getArgumentsTypes(f))) = 0;
                   if ($(functionPtrName(f)) == 0) {
                       eta_prevent_recursion_enter();
                       $(initPtr(f));
                       eta_prevent_recursion_exit();
                   }

                   $(ret_storage(f))$(functionPtrName(f))($(getArgumentsNames(f)));$(
                     if f.ret != "void"; "\n\n    return ret;" else "" end)
               }

               """) |> join)

    // static void init() __attribute__((constructor));
    // static void init() {
    //     printf("Init module $(m.name)\\n");
    // }

    // static void deinit() __attribute__((destructor));
    // static void deinit() {
    //     printf("Deinit module $(m.name)\\n");
    // }
    """ |> (data -> open(destination, "w") do output; write(output, data) end)

else
    @assert false
end

