#include <unistd.h>

#include <common/configuration.hpp>
#include <eta/core/ProgramOptions.hxx>

#include "config.h"
#include "modules.h"

auto constexpr success = 0;
auto constexpr argument_error = 1;
auto constexpr execve_error = 2;

auto get_application_path(char const * application_name, Configuration const & config) {
    auto path = std::array<char, 2048> {};

    if (application_name[0] == '/') {
        if (config.debug)
            std::cerr << '"' << application_name << "\" is absolute" << std::endl;
    } else if (application_name[0] == '.' && application_name[1] == '/') {
        if (config.debug)
            std::cerr << '"' << application_name << "\" is relative to current directory"
                      << std::endl;
    } else {
        auto path_env = static_cast<char const *>(getenv("PATH"));
        if (path_env == nullptr) {
            if (config.debug)
                std::cerr << "PATH is not set" << std::endl;
        } else {
            if (config.debug)
                std::cerr << "Search executable in PATH : " << path_env << std::endl;
            while (*path_env != '\0') {
                // Skip delimiters at begin of string
                while (*path_env != '\0' && *path_env == ':')
                    ++path_env;

                // If didn't reach end of string
                if (*path_env != '\0') {
                    // Copy the first path from path_env to path
                    auto index = 0u;
                    while (*path_env != '\0' && *path_env != ':') {
                        assert(index < path.size());
                        path[index++] = *(path_env++);
                    }

                    // add path separator
                    assert(index < path.size());
                    path[index++] = '/';

                    // add application name
                    auto it = application_name;
                    while (*it != '\0') {
                        assert(index < path.size());
                        path[index++] = *(it++);
                    }

                    // add null terminator
                    assert(index < path.size());
                    path[index++] = '\0';

                    // if file exists return the path found
                    if (access(path.data(), F_OK) >= 0) {
                        if (config.verbose)
                            std::cerr << "Found executable : \"" << path.data() << "\"."
                                      << std::endl;
                        return path;
                    }
                }
            }
        }
    }
    // if the path was relative to current dir, or if no path was found, return the
    // string as is
    std::strncpy(path.data(), application_name, path.size());
    return path;
}

auto main(int argc, char ** argv) -> int {
    // define options for the argument parser
    auto parser = po::parser {};
    parser.verbose(std::cerr);

    auto & help_opt = parser["help"]  //
                              .abbreviation('h')
                              .description("display this help")
                              .single();

    auto & verbose_opt = parser["verbose"]  //
                                 .abbreviation('v')
                                 .description("Enable extra information print")
                                 .single();

    auto & debug_opt = parser["debug"]  //
                               .abbreviation('d')
                               .description("Enable debug logs")
                               .single();

    for (auto const & m : eta_oracle_modules)
        parser[m.name].description(m.description).single();

    auto & all_modules_opt = parser["all"].description("Enable all modules").single();

    auto usage = [&]() {
        std::cout << "Usage of eta oracle" << std::endl;  // TODO
        std::cout << parser;
    };

    // seach for the name of the application to launch (first free argument)
    auto const application_index = [&]() {
        auto index = 1;
        while (index != argc) {
            if (strcmp(argv[index], "--") == 0)
                break;
            ++index;
        }
        return index + 1;
    }();

    // Parse the oracle options.  Only parse the first arguments. The next ones are for the
    // application to launch
    if (!parser(application_index - 1, argv))
        return argument_error;

    // Active the options
    if (help_opt.was_set()) {
        usage();
        return success;
    }

    // Check that the application was specified by the user

    if (application_index >= argc) {
        std::cerr << "Program to launch and options must be separated by -- from " << argv[0]
                  << " arguments" << std::endl;
        return argument_error;
    }

    if (application_index == argc) {
        std::cerr << "Missing program to launch" << std::endl;
        return argument_error;
    }

    auto config = Configuration {};

    config.debug = debug_opt.was_set();
    config.verbose = config.debug || verbose_opt.was_set();

    if (all_modules_opt.was_set()) {
        for (auto const & m : eta_oracle_modules)
            config.enabled_modules.push_back(m.name);
    } else {
        for (auto const & m : eta_oracle_modules)
            if (parser[m.name].was_set())
                config.enabled_modules.push_back(m.name);
    }

    config.ld_library_path = modules_prefix;
    setConfiguration(config);

    // Execute the application
    auto const application_path = get_application_path(argv[application_index], config);
    extern char ** environ;
    [[maybe_unused]] auto const ret =
            execve(application_path.data(), argv + application_index, environ);

    std::cerr << "Could not execve \"" << application_path.data() << "\" : " << errno << " - "
              << strerror(errno) << std::endl;
    return execve_error;
}

