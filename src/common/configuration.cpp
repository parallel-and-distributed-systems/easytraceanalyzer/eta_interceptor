#include "configuration.hpp"

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>

static auto debug_opt_name = "ETA_DEBUG_ENABLED";
static auto verbose_opt_name = "ETA_VERBOSE_ENABLED";
static auto ld_library_path_name = "LD_LIBRARY_PATH";
static auto ld_preload_name = "LD_PRELOAD";
static auto enabled_modules_opt_name = "ETA_ENABLED_MODULES";

static auto setOption(char const * name, bool b) -> void { setenv(name, (b ? "YES" : "NO"), 1); }

static auto getOption(char const * name) -> bool {
    auto const opt = getenv(name);
    if (opt != nullptr && strcmp(opt, "YES") == 0)
        return true;
    assert(strcmp(opt, "NO") == 0);
    return false;
}

static auto getVariable(char const * name) -> std::string {
    auto const opt = getenv(name);
    return opt == nullptr ? "" : opt;
}

auto setConfiguration(Configuration const & config) -> void {
    setOption(debug_opt_name, config.debug);
    setOption(verbose_opt_name, config.verbose);

    // LD_LIBRARY_PATH
    auto const old_ld_library_path = getVariable(ld_library_path_name);
    auto const new_ld_library_path = old_ld_library_path.empty()
                                             ? config.ld_library_path
                                             : old_ld_library_path + ":" + config.ld_library_path;
    setenv(ld_library_path_name, new_ld_library_path.c_str(), 1);

    // ETA_ENABLED_MODULES and LD_PRELOAD
    auto enabled_modules = std::stringstream {};
    auto ld_preload = std::stringstream {};

    auto const old_ld_preload = getVariable(ld_preload_name);
    if (old_ld_preload.size() > 0) {
        ld_preload << old_ld_preload;
        if (config.enabled_modules.size() > 0)
            ld_preload << ':';
    }

    for (auto i = 0u; i < config.enabled_modules.size(); ++i) {
        if (i > 0) {
            enabled_modules << ':';
            ld_preload << ':';
        }
        auto const & enabled_module = config.enabled_modules[i];
        enabled_modules << enabled_module;
        ld_preload << "libeta_oracle_" << enabled_module << ".so";
    }

    setenv(ld_preload_name, ld_preload.str().c_str(), 1);
    setenv(enabled_modules_opt_name, enabled_modules.str().c_str(), 1);
}

static auto loadConfiguration() -> Configuration {
    auto config = Configuration {};

    config.debug = getOption(debug_opt_name);
    config.verbose = getOption(verbose_opt_name);

    auto enabled_modules = std::stringstream { getVariable(enabled_modules_opt_name) };

    auto buf = std::string {};
    while (std::getline(enabled_modules, buf, ':')) {
        config.enabled_modules.emplace_back(std::move(buf));
    }

    return config;
}

auto getConfiguration() -> Configuration const * {
    static auto config = loadConfiguration();
    return &config;
}
