#include "recursion_guard.hpp"

namespace eta::interceptor {

thread_local unsigned int RecursionGuard::recursion_shield = 0u;

extern "C" {

void eta_prevent_recursion_enter(void) { RecursionGuard::enter_protected_section(); }

void eta_prevent_recursion_exit(void) { RecursionGuard::exit_protected_section(); }

int eta_is_in_recursion(void) {
    return static_cast<int>(RecursionGuard::is_in_protected_section());
}
}

}  // namespace eta::interceptor

