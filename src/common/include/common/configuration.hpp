#pragma once

#include <string>
#include <vector>

struct Configuration {
    bool debug;
    bool verbose;
    std::string ld_library_path;
    std::vector<std::string> enabled_modules;
};

auto setConfiguration(Configuration const &) -> void;
auto getConfiguration() -> Configuration const *;

