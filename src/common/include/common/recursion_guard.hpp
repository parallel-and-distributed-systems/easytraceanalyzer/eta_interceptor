#pragma once

namespace eta::interceptor {

struct RecursionGuard final {
    RecursionGuard() { ++recursion_shield; }
    ~RecursionGuard() { --recursion_shield; }

    auto is_in_recursion() { return recursion_shield > 1u; }

    static auto enter_protected_section() { ++recursion_shield; }
    static auto exit_protected_section() { --recursion_shield; }
    static auto is_in_protected_section() { return recursion_shield > 0u; }

  private:
    static thread_local unsigned int recursion_shield;
};

}  // namespace eta::interceptor

