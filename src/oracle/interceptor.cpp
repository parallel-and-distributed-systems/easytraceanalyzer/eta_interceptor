#include "interceptor.h"

#include <cassert>
#include <common/configuration.hpp>
#include <common/recursion_guard.hpp>

#include "tracing.hpp"

namespace eta::interceptor {

thread_local static auto thread_data = new_thread_data();

extern "C" {

void eta_intercept_event(FunctionEvent e) {
    assert(thread_data != nullptr);
    register_thread_event(thread_data, e);
}

// void init() __attribute__((constructor));
// void init() {}

static void deinit() __attribute__((destructor));
static void deinit() {
    eta_prevent_recursion_enter();  // forever
    dump_all_threads();
    destroy_all_thread_data();
}

}  // extern "C"

}  // namespace eta::interceptor

