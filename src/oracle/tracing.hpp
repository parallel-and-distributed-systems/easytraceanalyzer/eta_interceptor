#pragma once

#include <algorithm>
#include <common/recursion_guard.hpp>
#include <cstdio>
#include <cstring>
#include <eta/factorization/bin_file.hpp>
#include <eta/factorization/export.hpp>
#include <eta/factorization/factorization.hpp>
#include <fstream>
#include <mutex>
#include <sstream>
#include <vector>

#include "eta_oracle_events.h"
#include "events_names.h"

namespace eta::interceptor {

struct ThreadData {
    Grammar grammar;
    std::vector<Terminal *> terminals;
    NonTerminal * non_terminal = nullptr;

    ThreadData() { terminals.resize(eta_intercept_event_count, nullptr); }

    auto getTerminal(FunctionEvent e) {
        auto & t = terminals[static_cast<size_t>(e)];
        if (t == nullptr)
            t = new_terminal(grammar, reinterpret_cast<void *>(e));
        return t;
    }
};

static ThreadData * threads[1024];
static size_t thread_count = 0u;
static std::mutex threads_mtx;

auto new_thread_data() -> ThreadData * {
    assert(RecursionGuard::is_in_protected_section());
    auto lck = std::unique_lock(threads_mtx);
    auto const ptr = new ThreadData {};
    threads[thread_count++] = ptr;
    return ptr;
}

auto destroy_all_thread_data() -> void {
    assert(RecursionGuard::is_in_protected_section());
    auto lck = std::unique_lock(threads_mtx);
    for (auto i = 0u; i < thread_count; ++i)
        delete threads[i];
    thread_count = 0;
}

auto register_thread_event(ThreadData * data, FunctionEvent e) -> void {
    assert(RecursionGuard::is_in_protected_section());
    data->non_terminal = insertSymbol(data->grammar, data->non_terminal, data->getTerminal(e));
}

auto dump_all_threads() -> void {
    assert(RecursionGuard::is_in_protected_section());
    auto lck = std::unique_lock(threads_mtx);
    auto seed = std::rand() % 1000;

    auto printer = [](Terminal const * t, std::ostream & os) {
        os << get_event_name(*reinterpret_cast<FunctionEvent const *>(&t->payload)) << ' ';
    };

    for (auto i = 0u; i < thread_count; ++i) {
        auto const file_name = [](auto execution_index, auto thread_index) {
            auto ss = std::stringstream {};
            ss << execution_index << '_' << thread_index << ".btr";
            return ss.str();
        }(seed, i);

        auto os = std::ofstream {};
        os.open(file_name);
        print_bin_file(threads[i]->grammar, os, printer);
        os.close();
    }
}

}  // namespace eta::interceptor
