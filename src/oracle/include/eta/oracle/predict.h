#ifndef ETA_PREDICT_H
#define ETA_PREDICT_H

#ifdef __cplusplus
extern "C" {
#endif

struct EtaEvent;

struct EtaEvent * create_new_event();
struct EtaEvent * get_existing_event(char const * function_name);
void register_event(struct EventId);

bool is_oracle_active();
// TODO how to get prediction ?

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  // ETA_PREDICT_H
