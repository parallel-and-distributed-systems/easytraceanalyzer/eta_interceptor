#ifndef ETA_INTERCEPTOR_H
#define ETA_INTERCEPTOR_H

#include "eta_oracle_events.h"

#ifdef __cplusplus
extern "C" {
#endif

void eta_intercept_event(FunctionEvent);
void eta_prevent_recursion_exit(void);
void eta_prevent_recursion_enter(void);
int eta_is_in_recursion(void);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  // ETA_INTERCEPTOR_H
